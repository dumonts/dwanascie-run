<img src="https://gitlab.com/dumonts/dwanascie-run/-/raw/master/Dark-Logo.svg" alt="logo" width="250"/>

# Dwanaście Run

Run (single-tenant) kubernetes clusters on virtual or bare metal infrastructure.

Addons for kubernetes ([ytt/kapp](https://k14s.io/) templates).

## Installation order

1. d12k-system
2. d12k-auth
3. d12k-admin
4. d12k-certs
5. d12k-kubedb
6. d12k-kubedb-pg
7. d12k-pgadmin
8. d12k-s3
9. d12k-logs
10. d12k-metrics

## Core

### d12k-system

- [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets): encrypt secrets
- [kubernetes-secret-generator](https://github.com/mittwald/kubernetes-secret-generator): generate random secrets
- [kubernetes-replicator](https://github.com/mittwald/kubernetes-replicator): replicate secrets and configmaps across namespaces
- [Local Path Provisioner](https://github.com/rancher/local-path-provisioner): dynamic provisioning of local volumes
- [Traefik](https://containo.us/traefik/): ingress controller
- [Kubernetes Metrics Server](https://github.com/kubernetes-sigs/metrics-server): container resource metrics

### d12k-lb

- [MetalLB](https://metallb.universe.tf/) load-balancer controller for bare-metal/virtual clusters

### d12k-certs

- [cert-manager](https://cert-manager.io/): automatic x509 certificate management

### d12k-auth

- [dex](https://github.com/dexidp/dex): oidc provider

### d12k-admin

- [Forecastle](https://github.com/stakater/Forecastle): control panel to access deployed applications
- [Kubernetes Dashboard](https://github.com/kubernetes/dashboard): web-based ui for kubernetes
- [Traefik Dashboard](https://containo.us/traefik/): access web-based ui for traefik


## Observe

### d12k-logs 

* [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/)
  * Discovers log targets
  * Attaches labels to log streams
  * Pushes them to the Loki.

### d12k-logs-storage

- [Loki](https://github.com/grafana/loki): log aggregation

### d12k-metrics

- [kube-state-metrics](https://github.com/kubernetes/kube-state-metrics): cluster-level metrics
- [node-exporter](https://github.com/prometheus/node_exporter): node-level metrics
- [prometheus-operator](https://github.com/coreos/prometheus-operator): operator to manage prometheus instances

### d12k-metrics-storage

- [Thanos](https://thanos.io/): long term metrics storage

### d12k-alerts

- [Alertmanager](https://github.com/prometheus/alertmanager): handle alerts send by prometheus

### d12k-grafana

- [Grafana Operator](https://github.com/integr8ly/grafana-operator): manage grafana instances

### d12k-tracing

- [Jaeger](https://www.jaegertracing.io/): operator to manage jaeger distributed tracing


## DevOps

### d12k-registry

- [Harbor](https://goharbor.io/): cloud native repository, container registry

### d12k-repo

- [Nexus Repository OSS](https://de.sonatype.com/nexus-repository-oss): artifact repository


## Data

### d12k-kubedb

- [KubeDB](https://kubedb.com/): operator to manage database

### d12k-kubedb-pg

- [KubeDB](https://kubedb.com/docs/v0.12.0/guides/postgres/): manage postgresql instances using kubeDB

### d12k-pgadmin

- [PgAdmin](https://www.pgadmin.org/): web-based postgresql administration

### d12k-s3

- [Minio](https://min.io/): S3 object storage


