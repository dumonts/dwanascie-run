#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
shopt -s inherit_errexit

function log() {
  >&2 printf "%s \n" "${1}"
}

function log_error() {
    local -r red='\033[0;31m'
    local -r nocolor='\033[0m'
    >&2 printf "${red}%s${nocolor}\n" "${1}"
}

function error() {
  log_error "${1}"
  exit 1
}

function main() {

  local -r tmp_dir=$(mktemp -d)
  local idx=0
  local dst=""
  local url=""
  local ytt_args=""

  log "> fetch yaml sources"
  while IFS= read -r line || [[ -n "$line" ]]; do
    if [[ ! $line =~ \#.* ]]; then
      if [[ -d "${line}" ]]; then
        log ">> directory: $line"
        dst="${line}"
      elif [[ -f "${line}"  && ( $line =~ .*\.(yml|yaml) ) ]] ; then
        log ">> yaml file: $line"
        dst="${line}"
      else
        log ">> go-getter url: $line"
        dst="${tmp_dir}/${idx}"
        url="${line}"
        go-getter ${url} ${dst} > /dev/null
        idx=$((idx + 1))
      fi
      ytt_args="${ytt_args} -f ${dst}"
    fi
  done <"yttf.urls"

  log "> call ytt $ytt_args ${@}"
  ytt $ytt_args ${@}

  log "> remove tmp dir"
  rm -rf ${tmp_dir}

}

main "${@}"
