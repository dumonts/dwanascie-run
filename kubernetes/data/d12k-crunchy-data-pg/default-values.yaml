#@data/values
---

#! ctl

#! shut down all related resources.
shutdown: false

postgres_version: 14

node_port: false
namespace: "postgres-operator"
cluster_name: "a_cluster_name"
volume_name: "a_volume"

resources:
  requests:
    cpu: 2
    memory: 2Gi
  limits:
    memory: 4Gi

monitoring:
  enabled: false

#! use labels to determine fiting nodes
node_selector: 
  key: kubernetes.io/hostname
  val: my-db-worker

images: 
  postgres: registry.developers.crunchydata.com/crunchydata/crunchy-postgres:ubi8-14.5-1
  pgbackrest: registry.developers.crunchydata.com/crunchydata/crunchy-pgbackrest:ubi8-2.40-1
  monitoring: registry.developers.crunchydata.com/crunchydata/crunchy-postgres-exporter:ubi8-5.2.0-0

#! encryption is disabled by default. This requires a working cert manager instance.
tls: false

#! manage users for fresh clusters
user_management:
  create_initial_users: false
  users:
  - databases:
    - default
    name: default

#! database init helper. This requires a configmap matching the following selectors
#! kubectl -n { NS } create configmap init-sql --from-file=init.sql=/path/to/init.sql
#! The ConfigMap must exist in the same namespace as your Postgres cluster.
init_sql:
  execute_init_snippet: false 
  configmap_name: init-sql
  configmap_data_key: init.sql

#! backup related configuration settings
backup:
  #! change the verbosity of backup/restore jobs
  #! Possible vals https://pgbackrest.org/configuration.html#section-log/option-log-level-console
  loglevel: info
  #! number of processes contributing to backup/restore. Must be of type string
  workers: "1"
  
  #! it can be usefull to write and read wals to and from a fast and designated io device
  wals:
    #! put write ahead logs to a separate block device
    separate_storage: false
    #! the path the block device is mounted on
    mount_path: "/path/to/mount/"
  
  #! Schedule backups. Cron format "min hour day_of_month month day_of_week"
  #! https://crontab.guru
  schedules:
    local:
        #! keep n full backups on disk
        max_retention_in_count: "2"
        full: "0 0 * * 5"
        inc: "15 8-18 * * 1-5"
    remote:
        #! keep n full backups on remoute source
        max_retention_in_count: "4"
        full: "0 0 * * 6"
        inc: "0 20 * * *"
  #! persist backups to local disk
  local:
    enabled: true
  #! persist backups to a remote storage
  s3:
    #! write to or recover from remote storage?
    enabled: false
    #! kubeseal --raw --from-file=s3.conf --namespace postgres-operator --name s3-credentials | xclip -sel clip
    secrets: "<generate-me>"
    #! recover from this repo
    src:
      #! When this is true, the deployment will clone an existing remote source.
      #! Else init a new database cluster
      clone_existing: false
      #! S3 endpoint. Format: <endpoint>:<80>|<443>.
      endpoint: "my-secure-s3-endpoint:443"
      #! S3 region.
      region: "my-aws-region"
      #! buckets will not be initialized and have to be created manually in advance.
      bucket: "default"
    #! write to this repo
    dest:
      #! S3 endpoint. Format: <endpoint>:<80>|<443>.
      endpoint: "my-secure-s3-endpoint:443"
      #! S3 region.
      region: "my-aws-region"
      #! buckets will not be initialized and have to be created manually in advance.
      bucket: "default"
