load("@ytt:assert", "assert")
load("@ytt:data", "data")

data.values.backup.s3.enabled or data.values.backup.local.enabled or assert.fail("Either remote or local backup has to be toggled on")
