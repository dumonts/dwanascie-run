# Sealed Secrets Controller:

```
Info:

Version: v0.17.1
Sources 
  * https://bitnami-labs.github.io/sealed-secrets
  * helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets

When Updating:

Update kubeseal from https://github.com/bitnami-labs/sealed-secrets/releases
```

You should now be able to create sealed secrets.

1. Install the client-side tool (kubeseal) as explained in the docs below:

   https://github.com/bitnami-labs/sealed-secrets#installation-from-source

2. Create a sealed secret file running the command below:

   kubectl create secret generic secret-name --dry-run=client --from-literal=foo=bar -o [json|yaml] | \
   kubeseal \
   --controller-name=test-sealed-secrets \
   --controller-namespace=default \
   --format yaml > mysealedsecret.[json|yaml]

The file mysealedsecret.[json|yaml] is a commitable file.

If you would rather not need access to the cluster to generate the sealed secret you can run:

    kubeseal \
      --controller-name=test-sealed-secrets \
      --controller-namespace=default \
      --fetch-cert > mycert.pem

to retrieve the public cert used for encryption and store it locally. You can then run 'kubeseal --cert mycert.pem' instead to use the local cert e.g.

    kubectl create secret generic secret-name --dry-run=client --from-literal=foo=bar -o [json|yaml] | \
    kubeseal \
      --controller-name=test-sealed-secrets \
      --controller-namespace=default \
      --format [json|yaml] --cert mycert.pem > mysealedsecret.[json|yaml]

3. Apply the sealed secret

   kubectl create -f mysealedsecret.[json|yaml]

Running 'kubectl get secret secret-name -o [json|yaml]' will show the decrypted secret that was generated from the sealed secret.

Both the SealedSecret and generated Secret must have the same name and namespace.

4. Apply raw secrets

Creating temporary Secret with the kubectl command, only to throw it away once piped to kubeseal can be a quite unfriendly user experience.
We're working on an overhaul of the CLI experience.
In the meantime, we offer an alternative mode where kubeseal only cares about encrypting a value to stdout, 
and it's your responsibility to put it inside a SealedSecret resource (not unlike any of the other k8s resources).

Scopes
``` sh
# strict:
$ echo -n foo | kubeseal --raw --from-file=/dev/stdin --namespace bar --name mysecret
```


``` sh
# namespace-wide scope:
$ echo -n foo | kubeseal --raw --from-file=/dev/stdin --namespace bar --scope namespace-wide

# Include the sealedsecrets.bitnami.com/namespace-wide annotation in the SealedSecret
metadata:
 annotations:
  sealedsecrets.bitnami.com/namespace-wide: "true"

```

``` sh
# cluster-wide scope:
$ echo -n foo | kubeseal --raw --from-file=/dev/stdin --scope cluster-wide

# Include the sealedsecrets.bitnami.com/cluster-wide annotation in the SealedSecret
metadata:
  annotations:
    sealedsecrets.bitnami.com/cluster-wide: "true"
```